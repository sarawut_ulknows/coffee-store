/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;
import model.Product;
import model.Salary;

/**
 *
 * @author THANAWAT_TH
 */
public class Employeedao implements DaoInterface<Employee> {

    SaralyDao salaryDao = new SaralyDao();

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (Employee_name,Employee_Username,Employee_Password,Employee_account,Employee_nameBank)\n"
                    + "VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            stmt.setString(4, object.getAccount());
            stmt.setString(5, object.getBankName());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to add database");
        }

        db.close();
        Time start = java.sql.Time.valueOf("00:00:00");
        salaryDao.add(new Salary(-1, start, 0, object.getName(), object.getAccount(), object.getBankName()));
        return id;
    }

    public static void main(String[] args) {
        Employeedao dao = new Employeedao();
        System.out.println(dao.getAll());
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Employee_id,\n"
                    + "       Employee_name,\n"
                    + "       Employee_Username,\n"
                    + "       Employee_Password,\n"
                    + "       Employee_account,\n"
                    + "       Employee_nameBank\n"
                    + "  FROM Employee;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Employee_id");
                String name = result.getString("Employee_name");
                String username = result.getString("Employee_Username");
                String password = result.getString("Employee_Password");
                String account = result.getString("Employee_account");
                String bankName = result.getString("Employee_nameBank");

                Employee employee = new Employee(id, name, username, password, account, bankName);
                list.add(employee);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all Employee");
        }

        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_id,Employee_name,Employee_Username,Employee_Password,Employee_account,Employee_nameBank\n"
                    + "FROM Employee WHERE Employee_id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("Employee_id");
                String name = result.getString("Employee_name");
                String userName = result.getString("Employee_Username");
                String password = result.getString("Employee_Password");
                String account = result.getString("Employee_account");
                String bankName = result.getString("Employee_nameBank");
                Employee employee = new Employee(pid, name, userName, password, account, bankName);
                db.close();
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("error get id ");
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Employee\n"
                    + "      WHERE Employee_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Delete error!");
        }

        db.close();
        salaryDao.delete(id);
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Salary newSalary = null;
        int row = 0;
        try {

            String sql = "UPDATE Employee SET Employee_Name = ?, Employee_Username = ?, Employee_Password = ? ,Employee_account = ?,Employee_nameBank = ? WHERE Employee_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            stmt.setString(4, object.getAccount());
            stmt.setString(5, object.getBankName());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
            newSalary = new Salary(object.getId(), object.getName(), object.getAccount(), object.getBankName());

        } catch (SQLException ex) {
            System.out.println("error Update!");
        }

        db.close();
        salaryDao.update(newSalary);
        return row;
    }

}

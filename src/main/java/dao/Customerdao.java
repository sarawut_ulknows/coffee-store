/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author THANAWAT_TH
 */
public class Customerdao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO Customer (Customer_name,Customer_Phonenumber) VALUES (?,?);";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);

            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to Add Customer!!!!");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Customer_id,Customer_name,Customer_Phonenumber FROM Customer";
            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int Customer_id = result.getInt("Customer_id");
                String Customer_name = result.getString("Customer_name");
                String Customer_Phonenumber = result.getString("Customer_Phonenumber");

                Customer customer = new Customer(Customer_id, Customer_name, Customer_Phonenumber);
                list.add(customer);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Customerdao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Customer customer = null;

        try {
            String sql = "SELECT Customer_id,Customer_name,Customer_Phonenumber FROM Customer WHERE Customer_id=" + id;

            stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            if (result.next()) {

                int Customer_id = result.getInt("Customer_id");
                String Customer_name = result.getString("Customer_name");
                String Customer_Phonenumber = result.getString("Customer_Phonenumber");
                customer = new Customer(Customer_id, Customer_name, Customer_Phonenumber);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Customerdao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return customer;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        //Process
        String sql = "DELETE FROM Customer WHERE Customer_id = ?";
        try {

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to Delete!!!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            //Process
            String sql = "UPDATE Customer SET Customer_name = ?,Customer_Phonenumber = ? WHERE Customer_id = ?";

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to Update!!!!");
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        Customerdao dao = new Customerdao();
        dao.add(new Customer(-1, "Emi", "000000000"));
        System.out.println(dao.getAll());

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import model.Employee;
import model.Product;
import model.Receipt;
import model.Salary;

/**
 *
 * @author WIN10
 */
public class SaralyDao implements DaoInterface<Salary> {

    /*
    INSERT INTO Saraly_Table (Employee_id,TimesOfWorks,Employee_Salary)
    VALUES ('Employee_id');

     */
    @Override
    public int add(Salary object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO Salary_Table (Employee_name,TimesOfWorks,Employee_Salary,Employee_account,Employee_nameBank)\n"
                    + "    VALUES (?,?,?,?,?);";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setTime(2, object.getTime());
            stmt.setDouble(3, object.getSaraly());
            stmt.setString(4, object.getAccount());
            stmt.setString(5, object.getBankName());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: to add " + ex.getMessage());
        }

        db.close();
        return id;
    }

//    public static void main(String[] args) {
//        SaralyDao dao = new SaralyDao();
//        Time start = java.sql.Time.valueOf("00:00:00");
//        int num = dao.add(new Salary(start,0));
//        Salary update = dao.get(num);
//        System.out.println(dao.getAll());
//        update.setSaraly(90000);
//        dao.update(update);
//        System.out.println(dao.get(num));
//        System.out.println(dao.getAll());
//    }
//    public static void main(String[] args) {
//        SaralyDao dao = new SaralyDao();
//        Scanner kb = new Scanner(System.in);
//        long start = System.currentTimeMillis();
//
//        Salary ss = dao.get(7); // from SelectRow
//        java.sql.Time myTime = dao.get(7).getTime();
//        LocalTime localtime = myTime.toLocalTime();
//
//        int a = kb.nextInt();
//
//        long elapsedTimeMillis = System.currentTimeMillis() - start;
//
//        float elapsedTimeMin = elapsedTimeMillis / (1000F);
//
//        localtime = localtime.plusSeconds((long) elapsedTimeMin);
//
//        Time finishedTime = java.sql.Time.valueOf(localtime.toString());
//        ss.setTime(finishedTime);
//        //-----------------------------------------------------------
//        dao.update(ss);
//        System.out.println(dao.get(7));
//
//        System.out.println("================");
//        System.out.println(dao.getAll());
//
//    }
    void startTime() {
        long start = System.currentTimeMillis();
//        
    }

    void endTime() {
//        float elapsedTimeMin = elapsedTimeMillis / (60 * 1000F);
    }

    /*
    SELECT Employee_id,TimesOfWorks,Employee_Salary
    FROM Saraly_Table;
    
    
     */
    @Override
    public ArrayList<Salary> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "    SELECT Employee_id,Employee_name,TimesOfWorks,Employee_Salary,Employee_account,Employee_nameBank\n"
                    + "    FROM Salary_Table;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Employee_id");
                String name = result.getString("Employee_name");
                Time time = result.getTime("TimesOfWorks");
                double salary = result.getDouble("Employee_Salary");
                String account = result.getString("Employee_account");
                String bankName = result.getString("Employee_nameBank");
                Salary slr = new Salary(id, time, salary, name, account, bankName);
                list.add(slr);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all product");
        }
        db.close();
        return list;
    }

    @Override
    public Salary get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Employee_id,Employee_name,TimesOfWorks,Employee_Salary,Employee_account,Employee_nameBank\n"
                    + "    FROM Salary_Table\n"
                    + "    WHERE Employee_id =" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("Employee_id");
                String name = result.getString("Employee_name");
                Time time = result.getTime("TimesOfWorks");
                double salary = result.getDouble("Employee_Salary");
                String account = result.getString("Employee_account");
                String bankName = result.getString("Employee_nameBank");
                Salary salaryz = new Salary(pid, time, salary, name, account, bankName);
                db.close();
                return salaryz;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to get database id " + id);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Salary_Table\n"
                    + "      WHERE Employee_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete database id " + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Salary object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        PreparedStatement stmt = null;
        String sql = null;
        try {

            if (object.getTime() == null) {
                sql = "UPDATE Salary_Table SET Employee_name = ?,Employee_account = ?,Employee_nameBank = ?\n"
                        + "       WHERE Employee_id = ?;";
                stmt = conn.prepareStatement(sql);
                
                stmt.setString(1, object.getName());
                stmt.setString(2, object.getAccount());
                stmt.setString(3, object.getBankName());
                stmt.setInt(4, object.getEmployee_id());
            } else {
                sql = "UPDATE Salary_Table SET  Employee_name = ?, TimesOfWorks = ?,\n"
                        + "       Employee_Salary = ? ,Employee_account = ?,Employee_nameBank = ?"
                        + "       WHERE Employee_id = ?;";
                stmt = conn.prepareStatement(sql);
                
                stmt.setString(1, object.getName());
                stmt.setTime(2, object.getTime());
                stmt.setDouble(3, object.getSaraly());
                stmt.setString(4, object.getAccount());
                stmt.setString(5, object.getBankName());
                stmt.setInt(6, object.getEmployee_id());
            }
            
            row = stmt.executeUpdate();

            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to update database " + ex);
        }

        db.close();
        return row;
    }

}

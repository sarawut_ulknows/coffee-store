/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author THANAWAT_TH
 */
public class Productdao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Product (Product_name,Product_Price,Product_qty ) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getQty());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to add database");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Product_id,Product_name, Product_Price, Product_qty FROM Product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Product_id");
                String name = result.getString("Product_name");
                double price = result.getDouble("Product_price");

                int qty = result.getInt("Product_qty");
                Product product = new Product(id, name, price, qty);
                list.add(product);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all product");
        }

        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Product_id,Product_name,Product_price,Product_qty FROM Product WHERE Product_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("Product_id");
                String name = result.getString("Product_name");
                double price = result.getDouble("Product_price");
                int qty = result.getInt("Product_qty");
                Product product = new Product(pid, name, price, qty);
                db.close();
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to get database id " + id);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Product WHERE Product_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete database id " + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Product SET  Product_name = ?, Product_Price = ?,Product_qty = ? WHERE Product_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getQty());
            stmt.setInt(4, object.getId());
        
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to update database ");
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        Productdao dao = new Productdao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
     //   int id = dao.add(new Product(-1, "ice coffee", 50,1));
     //   System.out.println("id : " + id);
        
        Product lastProduct = dao.get(8);
        System.out.println("last product: "+lastProduct);
         lastProduct.setPrice(100);
          int row = dao.update(lastProduct);
    }
}

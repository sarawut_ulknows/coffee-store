/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import model.Stock;

/**
 *
 * @author User
 */
public class Stockdao implements DaoInterface<Stock>{

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try{
            String sql = "INSERT INTO Stock (Stock_name,Stock_amount,Stock_min) VALUES(?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            stmt.setInt(3, object.getMin());
            
            int row = stmt.executeUpdate();
            
            ResultSet result = stmt.getGeneratedKeys();
            
            if (result.next()) {
                id = result.getInt(1);
            }
        }catch(SQLException ex){
            System.out.println("Error: Unable to add database");
        }
        
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
                try {
                    String sql = "SELECT Stock_id,Stock_name,Stock_amount,Stock_min FROM Stock";
                    Statement stmt = conn.createStatement();
                    ResultSet result = stmt.executeQuery(sql);
                    while (result.next()) {
                        int id = result.getInt("Stock_id");
                        String name = result.getString("Stock_name");
                        int amount = result.getInt("Stock_amount");
                        int min = result.getInt("Stock_min");
                        
                        Stock stock = new Stock(id,name,amount,min);
                        if(stock.getAmount() <= stock.getMin()){
                            list.add(0, stock);
                        }else{
                             list.add(stock);
                        }
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all stock");
        }
                
//        Collections.sort(list, new Comparator<Stock>() {
//            @Override
//            public int compare(Stock o1, Stock o2) {
//                return Integer.compare(o1.getAmount(), o2.getAmount());
//            }
//            
//        } );

        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
                    String sql = "SELECT Stock_id,Stock_name,Stock_amount,Stock_min FROM Stock WHERE Stock_id = " + id;
                    Statement stmt = conn.createStatement();
                    ResultSet result = stmt.executeQuery(sql);
                    if (result.next()) {
                        int sid = result.getInt("Stock_id");
                        String name = result.getString("Stock_name");
                        int amount = result.getInt("Stock_amount");
                        int min = result.getInt("Stock_min");
                        Stock stock = new Stock(sid, name, amount,min);
                        db.close();
                        return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to get database id " + id);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        
        try {
            String sql = "DELETE FROM Stock WHERE Stock_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete database id " + id);
        }
        
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        
        try {
            String sql = "UPDATE Stock SET  Stock_name = ?, Stock_amount = ?,Stock_min = ? WHERE Stock_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            stmt.setInt(3, object.getMin());
            stmt.setInt(4, object.getId());
        
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: Unable to update database ");
        }

        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        Stockdao dao = new Stockdao();
        dao.add(new Stock(4, "aaaa", 20,5));
        System.out.println(dao.getAll());
        
//        System.out.println(dao.get(1));
//        
//        Stock lastStock = dao.get(3);
//        System.out.println("last Stock: " + lastStock);
//        lastStock.setAmount(30);
//        int row = dao.update(lastStock);
    }
}

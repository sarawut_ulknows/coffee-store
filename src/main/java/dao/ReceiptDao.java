/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author Sarawut@ulknows
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        // After Connect
        try {
            String sql = "INSERT INTO Receipt (\n"
                    + "                        Receipt_totalPrice,\n"
                    + "                        Receipt_change,\n"
                    + "                        Receipt_getmoney,\n"
                    + "                        Employee_id,\n"
                    + "                        Customer_id)"
                    + "                    VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, object.getTotal());
            stmt.setDouble(2, object.getReceive() - object.getTotal());
            stmt.setDouble(3, object.getReceive());
            stmt.setInt(4, object.getSeller().getId());
            if (object.getCustomer() != null && object.getSeller() != null) {
                stmt.setInt(5, object.getCustomer().getId());
            }

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }

            for (ReceiptDetail rD : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO ReceiptDetail (Receipt_id,Product_name,Product_price,Product_id,qty,total)\n"
                        + "VALUES (?,?,?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, rD.getReceipt().getId());
                stmtDetail.setString(2, rD.getProduct().getName());
                stmtDetail.setDouble(3, rD.getProduct().getPrice());
                stmtDetail.setInt(4, rD.getProduct().getId());
                stmtDetail.setDouble(5, rD.getQty());
                stmtDetail.setDouble(6, rD.getTotal());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    rD.setId(id);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error: to create receipt" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        // After Connect
        try {
            String sql = "SELECT rc.Receipt_id as id,\n"
                    + "       datetime(Receipt_date, 'localtime') as Receipt_date,\n"
                    + "       Receipt_totalPrice,\n"
                    + "                        Receipt_change,\n"
                    + "                        Receipt_getmoney,\n"
                    + "       e.Employee_id as emp_id,\n"
                    + "       e.Employee_name as emp_name,\n"
                    + "       c.Customer_id as cus_id,\n"
                    + "       c.Customer_name as cus_name,\n"
                    + "       c.Customer_Phonenumber as cus_Phonenumber\n"
                    + "  FROM Receipt rc,Employee e,Customer c\n"
                    + "   WHERE rc.Customer_id = c.Customer_id AND rc.Employee_id = e.Employee_id";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Receipt_date"));
                double totalPrice = result.getDouble("Receipt_totalPrice");

                double change = result.getDouble("Receipt_change");

                double receive = result.getDouble("Receipt_getmoney");

                int employeeID = result.getInt("emp_id");
                String employeeName = result.getString("emp_name");
                int customerID = result.getInt("cus_id");
                String customerName = result.getString("cus_name");
                String customerTel = result.getString("cus_Phonenumber");

                Receipt receipt = new Receipt(id, date, totalPrice,
                        new Employee(employeeID, employeeName),
                        new Customer(customerID, customerName, customerTel), receive, change);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!! " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        Receipt product = null;
        // After Connect

        try {
            String sql = "SELECT rc.Receipt_id as id,\n"
                    + "       datetime(Receipt_date, 'localtime') as Receipt_date,\n"
                    + "       Receipt_totalPrice,\n"
                    + "                        Receipt_change,\n"
                    + "                        Receipt_getmoney,\n"
                    + "       e.Employee_id as emp_id,\n"
                    + "       e.Employee_name as emp_name,\n"
                    + "       c.Customer_id as cus_id,\n"
                    + "       c.Customer_name as cus_name,\n"
                    + "       c.Customer_Phonenumber as cus_Phonenumber\n"
                    + "  FROM Receipt rc,Employee e,Customer c\n"
                    + "  WHERE rc.Receipt_id = ? AND rc.Customer_id = cus_id AND rc.Employee_id = emp_id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Receipt_date"));
                double totalPrice = result.getDouble("Receipt_totalPrice");
                double change = result.getDouble("Receipt_change");

                double receive = result.getDouble("Receipt_getmoney");
                int employeeID = result.getInt("emp_id");

                String employeeName = result.getString("emp_name");
                int customerID = result.getInt("cus_id");
                String customerName = result.getString("cus_name");
                String customerTel = result.getString("cus_Phonenumber");

                Receipt receipt = new Receipt(rid, date, totalPrice,
                        new Employee(employeeID, employeeName),
                        new Customer(customerID, customerName, customerTel),receive,change);

                getReceiptDetail(conn, rid, receipt);
                db.close();
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id " + id + " !!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!! " + ex.getMessage());
        }
        db.close();
        return null;

    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.ReceiptDetail_id as id,"
                + "Receipt_id,\n"
                + "p.Product_id as prod_id,\n"
                + "p.Product_name as prod_name,\n"
                + "p.Product_price as prod_price,\n"
                + "qty,\n"
                + "total \n"
                + "\n"
                + "FROM ReceiptDetail rd,product p\n"
                + "WHERE Receipt_id = ? AND rd.Product_id = p.Product_id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int receiptId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("prod_id");
            String productName = resultDetail.getString("prod_name");
            double productPrice = resultDetail.getDouble("prod_price");
            int productQty = resultDetail.getInt("qty");
            int total = resultDetail.getInt("total");
            Product products = new Product(productId, productName, productPrice, productQty);
            receipt.addReceiptDetail(receiptId, products, productQty);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        // After Connect
        try {
            String sql = "DELETE FROM Receipt\n"
                    + "      WHERE Receipt_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id " + id + " !!!");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        // After Connect
//        try {
//            String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        db.close();
//        return row;
        return 0;
    }

    public static void main(String[] args) {

    }
}

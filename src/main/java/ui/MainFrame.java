/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.SaralyDao;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import model.Employee;
import model.Salary;

/**
 *
 * @author Sarawut@ulknows
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    String screen;
    Stack panelList = new Stack();
    Employee employee;
    static int click = 0;

    public MainFrame() {
        initComponents();

        loadImage();
        hideInfoPanel();
        scrMain.setViewportView(new LoginPanel(this));

//        scrMain.setViewportView(new MainmenuPanel(this));
    }

    private void hideInfoPanel() {
        btnBack.setVisible(false);
        btnLogout.setVisible(false);
        lblInfoUser.setVisible(false);
        lblNavigation.setText("Login to continue.");
        this.setTitle("Coffee Store | Login");

    }

    private void showInfoPanel() {
        btnLogout.setVisible(true);
        lblInfoUser.setVisible(true);

    }

    private void changePnl(String panel) {
        String nav = "Mainmenu";
        String intersect = " | ";
        String title = "Coffee Store";

        if (panel.equals("Mainmenu")) {
            lblNavigation.setText(panel);
            this.setTitle(title + intersect + nav);
        } else {
            btnBack.setVisible(true);
            lblNavigation.setText(nav + intersect + panel);
            this.setTitle(title + intersect + panel);
        }
    }

    void switchToPOS() {
        changePnl("Point of sale");
        panelList.push("Point of sale");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new PointofsalePanel(employee));
    }

    void switchToCustomer() {
        changePnl("Customer");
        panelList.push("Customer");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new CustomerPanel());
    }

    void switchToReceipt() {
        changePnl("Receipt");
        panelList.push("Receipt");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new ReceiptPanel());
    }

    void switchToEmployee() {
        changePnl("Employee");
        panelList.push("Employee");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new EmployeePanel());
    }

    void switchToProduct() {
        changePnl("Product Management");
        panelList.push("Product Management");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new ProductPanel());
    }

    void switchToStock() {
        changePnl("Stock");
        panelList.push("Stock");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new StockPanel());
    }

    void switchToSalary() {
        changePnl("Salary Manager");
        panelList.push("Salary Manager");
        btnBack.setEnabled(true);
        scrMain.setViewportView(new SalaryPanel());
    }

    //user logged in
    void switchToMainFrame(Employee who) {
        employee = who;
        //MainFrame
        showInfoPanel();
        changePnl("Mainmenu");
        lblInfoUser.setText(employee.getName() + " | " + "Employee");
        panelList.push("Mainmenu");
        btnBack.setVisible(true);
        btnBack.setEnabled(false);
        scrMain.setViewportView(new MainmenuPanel(this));
        click = 1;
        
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                if (click == 1) {
                    JOptionPane.showMessageDialog(null, "Please Click Logout Before Closing !! ", "Warning", JOptionPane.WARNING_MESSAGE);
                    click=0;
                }
            }
        });

        //starttime
        countTime(who);

    }

    SaralyDao daoSaraly;
    long start;
    Salary ss;
    java.sql.Time myTime;
    LocalTime localtime;
    long elapsedTimeMillis;
    float elapsedTimeMin;
    Time finishedTime;

    boolean isNull;

    void countTime(Employee who) {
        isNull = false;
        System.out.println("startTime");
        daoSaraly = new SaralyDao();
        start = System.currentTimeMillis();
        ss = daoSaraly.get(who.getId());
        if (ss == null) {
            isNull = true;
            JOptionPane.showMessageDialog(null, "Your id and password Didn't have in salary..");

            return;
        }

        myTime = ss.getTime();
        localtime = myTime.toLocalTime();

    }

    void stopTime() {
        if (isNull == false) {
            System.out.println("Stoptime");
            elapsedTimeMillis = System.currentTimeMillis() - start;
            elapsedTimeMin = elapsedTimeMillis / (1000F);
            localtime = localtime.plusSeconds((long) elapsedTimeMin);
            finishedTime = java.sql.Time.valueOf(localtime);
            ss.setTime(finishedTime);
            daoSaraly.update(ss);
        } else {
            isNull = false;
        }
    }

    private void backToMainmenu() {
        scrMain.setViewportView(new MainmenuPanel(this));
        changePnl("Mainmenu");
        btnBack.setEnabled(false);
    }

    private void loadImage() {
        try {
            File file = new File("logo/logo.png");
            BufferedImage image = ImageIO.read(file);
            btnImage.setIcon(new ImageIcon(image));
        } catch (IOException ex) {
            Logger.getLogger(ProductPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        pnlMainHead = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        lblNavigation = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        lblInfoUser = new javax.swing.JLabel();
        btnImage = new javax.swing.JButton();
        scrMain = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(1376, 818));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        pnlMainHead.setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBack.setForeground(new java.awt.Color(153, 0, 0));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        lblNavigation.setBackground(new java.awt.Color(255, 255, 255));
        lblNavigation.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNavigation.setForeground(new java.awt.Color(0, 0, 0));
        lblNavigation.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNavigation.setText("Mainmenu");
        lblNavigation.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnLogout.setBackground(new java.awt.Color(255, 255, 255));
        btnLogout.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnLogout.setForeground(new java.awt.Color(153, 0, 0));
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        lblInfoUser.setBackground(new java.awt.Color(255, 255, 255));
        lblInfoUser.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblInfoUser.setForeground(new java.awt.Color(0, 0, 0));
        lblInfoUser.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblInfoUser.setText("CADS DEV | Manager");

        btnImage.setBackground(new java.awt.Color(255, 255, 255));
        btnImage.setForeground(new java.awt.Color(255, 255, 255));
        btnImage.setText("jButton1");
        btnImage.setMaximumSize(new java.awt.Dimension(235, 41));
        btnImage.setMinimumSize(new java.awt.Dimension(235, 41));
        btnImage.setPreferredSize(new java.awt.Dimension(235, 41));
        btnImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImageActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlMainHeadLayout = new javax.swing.GroupLayout(pnlMainHead);
        pnlMainHead.setLayout(pnlMainHeadLayout);
        pnlMainHeadLayout.setHorizontalGroup(
            pnlMainHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMainHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblNavigation, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(btnImage, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(289, 289, 289)
                .addComponent(lblInfoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlMainHeadLayout.setVerticalGroup(
            pnlMainHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNavigation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlMainHeadLayout.createSequentialGroup()
                        .addGroup(pnlMainHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlMainHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblInfoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnImage, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        scrMain.setBackground(new java.awt.Color(255, 255, 255));
        scrMain.setMaximumSize(new java.awt.Dimension(1356, 758));
        scrMain.setMinimumSize(new java.awt.Dimension(1356, 758));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1374, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(scrMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlMainHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 839, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlMainHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrMain, javax.swing.GroupLayout.PREFERRED_SIZE, 768, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        btnBack.setVisible(false);
        click=0;
        scrMain.setViewportView(new LoginPanel(this));
        hideInfoPanel();
        //stopTime
       
        System.out.println(click);
        stopTime();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        if (panelList.size() > 1) {
            btnBack.setEnabled(true);
            backToMainmenu();
            panelList.pop();
        }
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnImageActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnImage;
    private javax.swing.JButton btnLogout;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblInfoUser;
    private javax.swing.JLabel lblNavigation;
    private javax.swing.JPanel pnlMainHead;
    private javax.swing.JScrollPane scrMain;
    // End of variables declaration//GEN-END:variables
}

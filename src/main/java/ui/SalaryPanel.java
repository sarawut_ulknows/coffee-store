package ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import dao.Employeedao;
import dao.Productdao;
import dao.SaralyDao;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import model.Employee;
import model.Product;
import model.Salary;

/**
 *
 * @author captain
 */
public class SalaryPanel extends javax.swing.JPanel {

    Employeedao daoem = new Employeedao();
    SaralyDao daosaraly = new SaralyDao();
    private TableModel model;
    ArrayList<Employee> emList = daoem.getAll();
    ArrayList<Salary> salaryList = new ArrayList<>();
    ArrayList<Employee> emtry = new ArrayList<>();

    /**
     * Creates new form SalaryPanel
     */
    public SalaryPanel() {
        initComponents();
        loadTable();
        btnClear.setVisible(false);
    }

    public void loadTable() {
        SaralyDao dao = new SaralyDao();
        salaryList = dao.getAll();
        for (Salary salary : salaryList) {

            if (salary.getTime().getHours() > 0) {
                salary.setSaraly(salary.getTime().getHours() * 60 + (salary.getTime().getMinutes()));
            } else {
                salary.setSaraly((salary.getTime().getMinutes()));
            }

            daosaraly.update(salary);
        }
        jTable1.setRowHeight(25);
        model = new TableModel(salaryList);
        jTable1.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnPay = new javax.swing.JButton();
        edtSearch = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(0, 0, 0));
        setMaximumSize(new java.awt.Dimension(1346, 748));
        setPreferredSize(new java.awt.Dimension(1346, 748));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Salary management");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(398, 398, 398))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTable1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        btnPay.setBackground(new java.awt.Color(255, 255, 255));
        btnPay.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 18)); // NOI18N
        btnPay.setForeground(new java.awt.Color(0, 0, 0));
        btnPay.setText("Pay");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        edtSearch.setBackground(new java.awt.Color(255, 255, 255));
        edtSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        edtSearch.setForeground(new java.awt.Color(0, 0, 0));
        edtSearch.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        edtSearch.setText("Search");
        edtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtSearchFocusLost(evt);
            }
        });
        edtSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                edtSearchMouseClicked(evt);
            }
        });
        edtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtSearchKeyPressed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 18)); // NOI18N
        btnClear.setForeground(new java.awt.Color(0, 0, 0));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnClear)
                                .addGap(18, 18, 18)
                                .addComponent(edtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1279, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 29, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear)
                    .addComponent(btnPay))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 558, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed

        if (jTable1.getSelectedRow() > -1) {
            Salary newsalary;

            if (statusSearch == 0) {
                newsalary = salaryList.get(jTable1.getSelectedRow());
            } else {
                newsalary = who;
            }

            if (newsalary.getSaraly() != 0) {
                int reply = JOptionPane.showConfirmDialog(null, "Are you want to Pay Employee_id = " + newsalary.getEmployee_id()
                        + "\nBank Name: " + newsalary.getBankName() + "\nAccount number:" + newsalary.getAccount() + "\n MONEY =  "
                        + newsalary.getSaraly(), "Pay", JOptionPane.YES_NO_OPTION);

                if (reply == JOptionPane.YES_OPTION) {
                    Time start = java.sql.Time.valueOf("00:00:00");
                    newsalary.setTime(start);
                    newsalary.setSaraly(0);
                    daosaraly.update(newsalary);
                    loadTable();
                }
            } else {

                JOptionPane.showMessageDialog(null, "Employee name :" + newsalary.getName() + "\nSalary = 0", "Warning",
                        JOptionPane.WARNING_MESSAGE);

            }
        } else {

            JOptionPane.showMessageDialog(null, "You don't choose anything", "Warning", JOptionPane.WARNING_MESSAGE);
        }


    }//GEN-LAST:event_btnPayActionPerformed
    Salary who = null;
    int statusSearch = 0;
    private void edtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtSearchKeyPressed
        try {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                ArrayList<Salary> checkList = daosaraly.getAll();
                boolean checkHave = false;
                int employee_id = Integer.parseInt(edtSearch.getText());
                for (Salary employee : checkList) {
                    if (employee_id == employee.getEmployee_id()) {
                        checkHave = true;
                        who = employee;
                    }
                }
                if (checkHave == true) {
                    checkList = new ArrayList<>();
                    checkList.add(who);
                    model = new TableModel(checkList);
                    jTable1.setModel(model);
                    btnClear.setVisible(true);
                    statusSearch = 1;
                } else {
                    JOptionPane.showMessageDialog(null, "Employee ID: " + employee_id + " Not Found !!!");
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Your input Mismatch!! ", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_edtSearchKeyPressed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        btnClear.setVisible(false);
        statusSearch = 0;
        loadTable();

    }//GEN-LAST:event_btnClearActionPerformed

    private void edtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtSearchFocusLost
        edtSearch.setText("Search");
    }//GEN-LAST:event_edtSearchFocusLost

    private void edtSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_edtSearchMouseClicked
        edtSearch.setText("");
        edtSearch.requestFocus();
    }//GEN-LAST:event_edtSearchMouseClicked

    public void refreshTable() {
        SaralyDao dao = new SaralyDao();
        ArrayList<Salary> newList = dao.getAll();
        salaryList.clear();
        salaryList.addAll(newList);
        jTable1.revalidate();
        jTable1.repaint();
//        jTable1.clearSelection();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnPay;
    private javax.swing.JTextField edtSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
 private class TableModel extends AbstractTableModel {

        private final ArrayList<Salary> data;
        String[] columStrings = {"Employee_id", "Employee_Name", "TimeOfWorks", " Employee_salary", "Employee_nameBank", "Employee_account"};

        public TableModel(ArrayList<Salary> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 6;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Salary salary = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return salary.getEmployee_id();
            }

            if (columnIndex == 1) {
                return salary.getName();
            }

            if (columnIndex == 2) {
                return salary.getTime();
            }
            if (columnIndex == 3) {
                return salary.getSaraly();
            }
            if (columnIndex == 4) {
                return salary.getBankName();

            }
            if (columnIndex == 5) {
                return salary.getAccount();
            }

            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columStrings[column];
        }

    }

}

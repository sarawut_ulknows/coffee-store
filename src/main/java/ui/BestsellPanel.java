/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.ReceiptDao;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import model.Receipt;
import model.ReceiptDetail;


/**
 *
 * @author THANAWAT_TH
 */
public class BestsellPanel extends javax.swing.JPanel {

    /**
     * Creates new form BadsellPanel
     */
    private BestsellPanel.TableModel model;
    ArrayList<String> sorted = new ArrayList<>();
    ArrayList<String> counted ;
    
    public BestsellPanel() {
        initComponents();
        makeArrayList();
        loadTable();
        tblBestsell.getColumnModel().getColumn(0).setPreferredWidth(10);
        tblBestsell.getColumnModel().getColumn(1).setPreferredWidth(160);
        tblBestsell.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblBestsell.setRowHeight(25);
        tblBestsell.setEnabled(false);
        
    }
    
    void loadTable(){
        model = new TableModel(counted,sorted);
        tblBestsell.setModel(model);
    }
    
    void makeArrayList(){
        ReceiptDao dao = new ReceiptDao();
        ArrayList<ReceiptDetail> rd = new ArrayList<>();
        int numOfReceipt = dao.getAll().size();
        System.out.println(numOfReceipt);

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        
        if(numOfReceipt==0){
            return;
        }
        
        for (int i = 1; i <= numOfReceipt; i++) {
            int numOfReceiptDetails = dao.get(i).getReceiptDetail().size();

            for (int j = 0; j < numOfReceiptDetails; j++) {
                String name = dao.get(i).getReceiptDetail().get(j).getProduct().getName();
                int qtyz = dao.get(i).getReceiptDetail().get(j).getQty();
                if (map.keySet().contains(name)) {
                    map.put(name, map.get(name) + qtyz);
                } else {
                    map.put(name, qtyz);
                }
            }
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey();
            Integer val = entry.getValue();
            System.out.println("Names: " + key);
            System.out.println("qty :" + val);
        }

//sorted
        ValueComparator bvc = new ValueComparator(map);
        TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(bvc);
        sorted_map.putAll(map);
        System.out.println("results: " + sorted_map);
//-------------------------------------------------------
         counted = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : sorted_map.entrySet()) {
            String key = entry.getKey();
            Integer val = entry.getValue();
            sorted.add(key);
             if(count < sorted.size()){
                count++;
                counted.add(count +"");
            }
        }
    }
    


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtName = new javax.swing.JLabel();
        pnlBestsell = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBestsell = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtName.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        txtName.setForeground(new java.awt.Color(255, 255, 255));
        txtName.setText("Bestsell");
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, -1, 40));

        pnlBestsell.setBackground(new java.awt.Color(255, 255, 255));
        pnlBestsell.setPreferredSize(new java.awt.Dimension(350, 400));

        tblBestsell.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        tblBestsell.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Rank", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBestsell.setPreferredSize(new java.awt.Dimension(350, 400));
        tblBestsell.getTableHeader().setReorderingAllowed(false);
        tblBestsell.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBestsellMouseClicked(evt);
            }
        });
        tblBestsell.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblBestsellKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblBestsell);
        if (tblBestsell.getColumnModel().getColumnCount() > 0) {
            tblBestsell.getColumnModel().getColumn(0).setPreferredWidth(1);
            tblBestsell.getColumnModel().getColumn(1).setPreferredWidth(200);
        }

        javax.swing.GroupLayout pnlBestsellLayout = new javax.swing.GroupLayout(pnlBestsell);
        pnlBestsell.setLayout(pnlBestsellLayout);
        pnlBestsellLayout.setHorizontalGroup(
            pnlBestsellLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBestsellLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlBestsellLayout.setVerticalGroup(
            pnlBestsellLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBestsellLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        add(pnlBestsell, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, -1, 350));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\THANAWAT_TH\\Documents\\NetBeansProjects\\coffee-store\\logo\\Back1.jpg")); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void tblBestsellMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBestsellMouseClicked

    }//GEN-LAST:event_tblBestsellMouseClicked

    private void tblBestsellKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblBestsellKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tblBestsellKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlBestsell;
    private javax.swing.JTable tblBestsell;
    private javax.swing.JLabel txtName;
    // End of variables declaration//GEN-END:variables

    int count = 0;
    int x = 0;
    void Count(){
        if(count >= x){
            
        }else{
            count++;
        }
        
    }
    
private class TableModel extends AbstractTableModel {
        
        private final ArrayList<String> data;
        private final ArrayList<String> data2;
        
        String[] columName = {"Ranked","Name"};
        
        public TableModel(ArrayList<String> data,ArrayList<String> data2) {
            this.data = data;
            this.data2 = data2;
        }
        @Override
        public int getRowCount() {
            x = this.data.size();
            return this.data.size();
        }
        @Override
        public int getColumnCount() {
            return 2;
        }
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            String test = this.data.get(rowIndex);
            String test2 = this.data2.get(rowIndex);

            if(columnIndex == 0){
                return test.toString();
            }
            
            
            if(columnIndex == 1){
                return test2.toString();
            }
            return null;
        }
        @Override
        public String getColumnName(int column) {
            return columName[column];
        }
    }

class ValueComparator implements Comparator<String> {

    Map<String, Integer> base;

    public ValueComparator(Map<String, Integer> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with
    // equals.
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }
}
}

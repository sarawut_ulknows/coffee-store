package ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import dao.Employeedao;
import dao.SaralyDao;
import java.awt.event.KeyEvent;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import model.Employee;
import model.Product;
import model.Salary;

/**
 *
 * @author WIN10
 */
public class EmployeePanel extends javax.swing.JPanel {

    Employee editedEmployee;
    Employee who = null;
    int statusSearch = 0;

    /**
     * Creates new form EmployeePanel
     */
    private ArrayList<Employee> employeeList;
    private EmployeeTableModel model;

    public EmployeePanel() {
        initComponents();
        loadTable();
        clearEdtForm();

    }

    void clearEdtForm() {
        edtId.setEnabled(false);
        edtName.setEnabled(false);
        edtUsername.setEnabled(false);
        edtPassword.setEnabled(false);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        btnClear.setVisible(false);

        btnClear.setVisible(false);
        edtSearch.setText("Search");
        edtName.setText("");
        edtUsername.setText("");
        edtPassword.setText("");
        edtId.setText("?");

        edtAccount.setEnabled(false);
        edtNamebank.setEnabled(false);

    }

    void enableEdtForm() {
        edtId.setEnabled(true);
        edtName.setEnabled(true);
        edtUsername.setEnabled(true);
        edtPassword.setEnabled(true);
        btnSave.setEnabled(true);
        btnCancel.setEnabled(true);

        btnEdit1.setEnabled(true);
        btnDelete1.setEnabled(true);
        edtAccount.setEnabled(true);
        edtNamebank.setEnabled(true);
    }

    public void refreshTable() {
        Employeedao dao = new Employeedao();
        ArrayList<Employee> newList = dao.getAll();
        employeeList.clear();
        employeeList.addAll(newList);
        tblEmployee.revalidate();
        tblEmployee.repaint();
    }

    public void loadTable() {
        Employeedao dao = new Employeedao();
        employeeList = dao.getAll();
        model = new EmployeeTableModel(employeeList);
        tblEmployee.setModel(model);
        tblEmployee.setRowHeight(25);
        statusSearch = 0;
    }

    boolean loadFromToEmployee() {
        if (statusSearch == 0) {

            if (edtName.getText().equals("") || edtUsername.getText().equals("") || edtPassword.getText().equals("") || edtAccount.getText().equals("") || edtNamebank.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Your did not enter all the information.\nPlease input again. ", "Warning", JOptionPane.WARNING_MESSAGE);
                loadTable();
                return true;
            }

            editedEmployee.setName(edtName.getText());
            editedEmployee.setUsername(edtUsername.getText());
            editedEmployee.setPassword(edtPassword.getText());
            editedEmployee.setAccount(edtAccount.getText());
            editedEmployee.setBankName(edtNamebank.getText());
        } else {
            who.setName(edtName.getText());
            who.setUsername(edtUsername.getText());
            who.setPassword(edtPassword.getText());
            who.setAccount(edtAccount.getText());
            who.setBankName(edtNamebank.getText());
        }
        return false;
    }

    public void loadEmployeeToFrom() {
        if (statusSearch == 0) {
            if (editedEmployee.getId() >= 0) {
                edtId.setText(editedEmployee.getId() + "");
                edtName.setText(editedEmployee.getName());
                edtUsername.setText(editedEmployee.getUsername() + "");
                edtPassword.setText(editedEmployee.getPassword() + "");
                edtAccount.setText(editedEmployee.getAccount() + "");
                edtNamebank.setText(editedEmployee.getBankName() + "");
            }
        } else {
            edtId.setText(who.getId() + "");
            edtName.setText(who.getName() + "");
            edtUsername.setText(who.getUsername() + "");
            edtPassword.setText(who.getPassword() + "");
            edtAccount.setText(who.getAccount() + "");
            edtNamebank.setText(who.getBankName() + "");

        }
        enableEdtForm();
    }

    public void clearEditForm() {
        editedEmployee = null;
        edtId.setText("?");
        edtName.setText("");
        edtUsername.setText("");
        edtPassword.setText("");
        edtAccount.setText("");
        edtNamebank.setText("");
        lblId.setEnabled(false);
        edtName.setEnabled(false);
        edtUsername.setEnabled(false);
        edtPassword.setEnabled(false);
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
        edtAccount.setEnabled(false);
        edtNamebank.setEnabled(false);
        tblProduct.clearSelection();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        srcProductPane = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        edtName = new javax.swing.JTextField();
        edtUsername = new javax.swing.JTextField();
        edtPassword = new javax.swing.JTextField();
        edtId = new javax.swing.JLabel();
        lblSection = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnAdd1 = new javax.swing.JButton();
        btnEdit1 = new javax.swing.JButton();
        btnDelete1 = new javax.swing.JButton();
        srcProductPane1 = new javax.swing.JScrollPane();
        tblEmployee = new javax.swing.JTable();
        edtSearch = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();
        edtNamebank = new javax.swing.JTextField();
        lblNamebank = new javax.swing.JLabel();
        lblAccount = new javax.swing.JLabel();
        edtAccount = new javax.swing.JTextField();

        btnAdd.setBackground(new java.awt.Color(204, 0, 204));
        btnAdd.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnAdd.setText("Add");

        btnEdit.setBackground(new java.awt.Color(204, 0, 204));
        btnEdit.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnEdit.setText("Edit");

        btnDelete.setBackground(new java.awt.Color(204, 0, 204));
        btnDelete.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnDelete.setText("Delete");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        srcProductPane.setViewportView(tblProduct);

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(1346, 748));
        setPreferredSize(new java.awt.Dimension(1346, 748));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblId.setBackground(new java.awt.Color(255, 255, 255));
        lblId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblId.setForeground(new java.awt.Color(0, 0, 0));
        lblId.setText("iD:");

        lblName.setBackground(new java.awt.Color(255, 255, 255));
        lblName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblName.setForeground(new java.awt.Color(0, 0, 0));
        lblName.setText("Name :");

        lblUsername.setBackground(new java.awt.Color(255, 255, 255));
        lblUsername.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUsername.setForeground(new java.awt.Color(0, 0, 0));
        lblUsername.setText("Username :");

        lblPassword.setBackground(new java.awt.Color(255, 255, 255));
        lblPassword.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPassword.setForeground(new java.awt.Color(0, 0, 0));
        lblPassword.setText("Password :");

        edtName.setBackground(new java.awt.Color(255, 255, 255));
        edtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        edtName.setForeground(new java.awt.Color(0, 0, 0));
        edtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtNameActionPerformed(evt);
            }
        });

        edtUsername.setBackground(new java.awt.Color(255, 255, 255));
        edtUsername.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        edtUsername.setForeground(new java.awt.Color(0, 0, 0));
        edtUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtUsernameActionPerformed(evt);
            }
        });

        edtPassword.setBackground(new java.awt.Color(255, 255, 255));
        edtPassword.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        edtPassword.setForeground(new java.awt.Color(0, 0, 0));

        edtId.setBackground(new java.awt.Color(255, 255, 255));
        edtId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        lblSection.setBackground(new java.awt.Color(255, 255, 255));
        lblSection.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        lblSection.setForeground(new java.awt.Color(0, 0, 0));
        lblSection.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSection.setText("Employee management");

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 0, 0));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 255, 255));
        btnCancel.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 14)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(0, 0, 0));
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        btnAdd1.setBackground(new java.awt.Color(255, 255, 255));
        btnAdd1.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnAdd1.setForeground(new java.awt.Color(0, 0, 0));
        btnAdd1.setText("Add");
        btnAdd1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdd1ActionPerformed(evt);
            }
        });

        btnEdit1.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit1.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnEdit1.setForeground(new java.awt.Color(0, 0, 0));
        btnEdit1.setText("Edit");
        btnEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEdit1ActionPerformed(evt);
            }
        });

        btnDelete1.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete1.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnDelete1.setForeground(new java.awt.Color(0, 0, 0));
        btnDelete1.setText("Delete");
        btnDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelete1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(btnAdd1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnEdit1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDelete1)
                .addContainerGap(626, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAdd1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdit1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        srcProductPane1.setBackground(new java.awt.Color(204, 204, 204));
        srcProductPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        srcProductPane1.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N

        tblEmployee.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        tblEmployee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEmployeeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tblEmployeeMouseEntered(evt);
            }
        });
        srcProductPane1.setViewportView(tblEmployee);

        edtSearch.setBackground(new java.awt.Color(255, 255, 255));
        edtSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        edtSearch.setForeground(new java.awt.Color(0, 0, 0));
        edtSearch.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        edtSearch.setText("Search");
        edtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtSearchFocusLost(evt);
            }
        });
        edtSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                edtSearchMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                edtSearchMouseExited(evt);
            }
        });
        edtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtSearchActionPerformed(evt);
            }
        });
        edtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                edtSearchKeyPressed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        btnClear.setForeground(new java.awt.Color(0, 0, 0));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        edtNamebank.setBackground(new java.awt.Color(255, 255, 255));
        edtNamebank.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        edtNamebank.setForeground(new java.awt.Color(0, 0, 0));
        edtNamebank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtNamebankActionPerformed(evt);
            }
        });

        lblNamebank.setBackground(new java.awt.Color(255, 255, 255));
        lblNamebank.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNamebank.setForeground(new java.awt.Color(0, 0, 0));
        lblNamebank.setText("Namebank :");

        lblAccount.setBackground(new java.awt.Color(255, 255, 255));
        lblAccount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblAccount.setForeground(new java.awt.Color(0, 0, 0));
        lblAccount.setText("Account :");

        edtAccount.setBackground(new java.awt.Color(255, 255, 255));
        edtAccount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        edtAccount.setForeground(new java.awt.Color(0, 0, 0));
        edtAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtAccountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSection, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(edtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(44, 44, 44)
                            .addComponent(srcProductPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1247, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(345, 345, 345)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblUsername)
                            .addComponent(lblId)
                            .addComponent(lblPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(edtId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(edtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(edtAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblNamebank))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(edtNamebank, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(526, 526, 526)))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSection)
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(edtId, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblName)
                        .addComponent(edtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNamebank)
                            .addComponent(lblUsername)
                            .addComponent(edtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPassword)
                            .addComponent(edtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAccount)
                            .addComponent(edtAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(edtNamebank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnCancel))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(edtSearch)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)))
                .addComponent(srcProductPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void edtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtNameActionPerformed

    private void edtUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtUsernameActionPerformed

    private void btnAdd1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdd1ActionPerformed
        enableEdtForm();
        editedEmployee = new Employee(-1, "", "", "");
        if (statusSearch == 1) {
            loadTable();
            btnClear.setVisible(false);
            statusSearch = 0;
        }
        edtName.requestFocus();
    }//GEN-LAST:event_btnAdd1ActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (loadFromToEmployee()) {
            return;
        }
        Employeedao dao = new Employeedao();

        if (statusSearch == 0) {
            if (editedEmployee.getId() == -1) {
                dao.add(editedEmployee);

                //salary
            } else {
                dao.update(editedEmployee);
            }
        } else {
            dao.update(who);
            loadTable();
            statusSearch = 0;
            clearEdtForm();
        }
        refreshTable();
        clearEditForm();

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEdit1ActionPerformed

        if (tblEmployee.getSelectedRow() > -1) {
            enableEdtForm();
            editedEmployee = employeeList.get(tblEmployee.getSelectedRow());
            loadEmployeeToFrom();
            btnClear.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(null, "You don't choose anything", "Warning", JOptionPane.WARNING_MESSAGE);
        }


    }//GEN-LAST:event_btnEdit1ActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearEditForm();
        if (statusSearch == 1) {
            statusSearch = 0;
            loadTable();
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelete1ActionPerformed

        Employeedao dao = new Employeedao();
        if (tblEmployee.getSelectedRow() > -1) {
            int reply = JOptionPane.showConfirmDialog(null, "Are you sure to delete!!!", "Warning!!", JOptionPane.YES_NO_OPTION);
            if (statusSearch == 0) {
                if (reply == JOptionPane.YES_OPTION) {
                    Employee wantdelete = employeeList.get(tblEmployee.getSelectedRow());

                    editedEmployee = wantdelete;
                    if (tblEmployee.getSelectedColumn() > -1) {
                        dao.delete(editedEmployee.getId());

                    }
                    refreshTable();
                    loadTable();
                }
            } else {
                if (reply == JOptionPane.YES_OPTION) {
                    dao.delete(who.getId());
                    who = null;
                    statusSearch = 0;
                    clearEdtForm();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "You don't choose anything", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnDelete1ActionPerformed

    private void edtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtSearchKeyPressed

        try {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

                Employeedao dao = new Employeedao();

                ArrayList<Employee> checkList = dao.getAll();

                boolean checkHave = false;

                int employee_id = Integer.parseInt(edtSearch.getText());

                for (Employee employee : checkList) {
                    if (employee_id == employee.getId()) {
                        checkHave = true;
                        who = employee;
                    }
                }

                if (checkHave == true) {
                    checkList = new ArrayList<>();
                    checkList.add(who);
                    model = new EmployeeTableModel(checkList);
                    tblEmployee.setModel(model);
                    btnClear.setVisible(true);
                    statusSearch = 1;
                } else {
                    JOptionPane.showMessageDialog(null, "Employee ID: " + employee_id + " Not Found !!!");
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Your input Mismatch!! ", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_edtSearchKeyPressed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        loadTable();
        clearEdtForm();
    }//GEN-LAST:event_btnClearActionPerformed

    private void edtSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_edtSearchMouseClicked
        edtSearch.setText("");
        edtSearch.requestFocus();
    }//GEN-LAST:event_edtSearchMouseClicked

    private void edtSearchMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_edtSearchMouseExited


    }//GEN-LAST:event_edtSearchMouseExited

    private void edtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtSearchFocusLost
        edtSearch.setText("Search");
    }//GEN-LAST:event_edtSearchFocusLost

    private void tblEmployeeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmployeeMouseEntered

    }//GEN-LAST:event_tblEmployeeMouseEntered

    private void tblEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmployeeMouseClicked

    }//GEN-LAST:event_tblEmployeeMouseClicked

    private void edtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtSearchActionPerformed

    private void edtNamebankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtNamebankActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtNamebankActionPerformed

    private void edtAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtAccountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtAccountActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAdd1;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDelete1;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnEdit1;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtAccount;
    private javax.swing.JLabel edtId;
    private javax.swing.JTextField edtName;
    private javax.swing.JTextField edtNamebank;
    private javax.swing.JTextField edtPassword;
    private javax.swing.JTextField edtSearch;
    private javax.swing.JTextField edtUsername;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblAccount;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNamebank;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblSection;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JScrollPane srcProductPane;
    private javax.swing.JScrollPane srcProductPane1;
    private javax.swing.JTable tblEmployee;
    private javax.swing.JTable tblProduct;
    // End of variables declaration//GEN-END:variables
    private class EmployeeTableModel extends AbstractTableModel {

        private final ArrayList<Employee> data;
        String[] columName = {"Employee_id", "Employee_name", "Employee_Username", "Employee_password", "Employee_nameBank", "Employee_account"};

        public EmployeeTableModel(ArrayList<Employee> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 6;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Employee employee = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return employee.getId();
            }
            if (columnIndex == 1) {
                return employee.getName();
            }

            if (columnIndex == 2) {
                return employee.getUsername();
            }

            if (columnIndex == 3) {
                return employee.getPassword();
            }

            if (columnIndex == 4) {
                return employee.getBankName();
            }

            if (columnIndex == 5) {
                return employee.getAccount();
            }

            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columName[column];
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Sarawut@ulknows
 */
public class Receipt {

    private int id;
    private Date created;
    private double totalPrice;
    private double receive;
    private double change;
    private Employee seller;
    private Customer customer;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int id, Date created, double totalPrice, Employee seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.totalPrice = totalPrice;

        this.seller = seller;
        this.customer = customer;
        this.receiptDetail = new ArrayList<>();
    }
    
       public Receipt(int id, Date created, double totalPrice, Employee seller, Customer customer,double receive, double change) {
        this.id = id;
        this.created = created;
        this.totalPrice = totalPrice;
        this.receive = receive;
        this.change = change;
        this.seller = seller;
        this.customer = customer;
        this.receiptDetail = new ArrayList<>();
    }

    public Receipt(Employee seller, Customer customer,double receive, double change) {
        this(-1, null, 0, seller, customer,receive,change);
    }

    public void addReceiptDetail(int id, Product product,int qty) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addQuantity(qty);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(id, product, qty,product.getPrice(), this));
    }

    public void addReceiptDetail(Product product,int qty) {
        addReceiptDetail(-1, product,qty);
    }

    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);
    }

    public double getTotal() {
        double total = 0;
        for (ReceiptDetail r : receiptDetail) {
            total += r.getTotal();
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Employee getSeller() {
        return seller;
    }

    public void setSeller(Employee seller) {
        this.seller = seller;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getReceive() {
        return receive;
    }

    public void setReceive(double receive) {
        this.receive = receive;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id
                + ", created=" + created
                + ", seller=" + seller
                + ", customer=" + customer
                + ", total=" + this.getTotal()
                + "}\n";
        for (ReceiptDetail r : receiptDetail) {
            str = str + r.toString() + "\n";
        }
        return str;
    }

    public static void main(String[] args) {
    }
}

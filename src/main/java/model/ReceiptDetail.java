/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sarawut@ulknows
 */
public class ReceiptDetail {

    private int id;
    private Product product;
    private double price;
    private int qty;
    private double amount;

    private Receipt receipt;

    public ReceiptDetail(int id, Product product, int qty, double price , Receipt receipt) {
        this.id = id;
        this.product = product;
        this.price = price;
        this.qty = qty;
        this.receipt = receipt;
       
    }

    public ReceiptDetail(Product product, double price, int qty, Receipt receipt) {
        this(-1, product, qty,price , receipt);
    }

    public double getTotal() {
        return qty*price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    
    public double getAmount() {
        return getTotal();
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void addQuantity(int qty) {
        this.qty += qty;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id
                + ", product=" + product
                + ", Quantity=" + qty
                + ", price =" + price
                + ", total=" + this.getTotal()
                + '}';
    }
}

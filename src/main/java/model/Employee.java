/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author THANAWAT_TH
 */
public class Employee {

   
    int id;
    String name;
    String username;
    String password;
    String account;
    String bankName;
    

    public Employee(int id, String name, String username, String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }
    
     public Employee(int id, String name, String username, String password, String account, String bankName) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.account = account;
        this.bankName = bankName;
    }
    
    
    public Employee(int id, String name) {
       this(id,name,"","");
    }
    
    
    public Employee(String name, String username, String password){
        this(-1,name,username,password);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
       public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String Username) {
        this.username = Username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Employee{name=" + name + '}';
    }

}

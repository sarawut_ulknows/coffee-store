/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Time;

/**
 *
 * @author WIN10
 */
public class Salary {

    int employee_id;
    Time time;
    double saraly;
    String name;
    String account;
    String bankName;
    
    public Salary(int employee_id, String name, String account,String bankName) {
        this.employee_id = employee_id;
        this.name = name;
        this.account=account;
        this.bankName=bankName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Salary(int employee_id, Time time, double saraly, String name) {
        this.employee_id = employee_id;
        this.time = time;
        this.saraly = saraly;
        this.name = name;
    }

    public Salary(int employee_id, Time time, double saraly, String name, String account, String bankName) {
        this.employee_id = employee_id;
        this.time = time;
        this.saraly = saraly;
        this.name = name;
        this.account = account;
        this.bankName = bankName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Salary(Time time, double saraly) {
        this.employee_id = employee_id;
        this.time = time;
        this.saraly = saraly;
    }

    public Salary(int id, Time time, double saraly) {
        this.employee_id = id;
        this.time = time;
        this.saraly = saraly;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public Time getTime() {
        return time;
    }

    public double getSaraly() {
        return saraly;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setSaraly(double saraly) {
        this.saraly = saraly;
    }

    @Override
    public String toString() {
        return "Saraly{" + "employee_id=" + employee_id + "name= " + name + ", time=" + time + ", saraly=" + saraly + '}';
    }

}

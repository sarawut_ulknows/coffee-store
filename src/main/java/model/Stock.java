/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class Stock {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Stock(int id, String name, int amount, int min) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.min = min;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", amount=" + amount + ", min=" + min + '}';
    }

    
    private int id;
    private String name;
    private int amount;
    private int min;
    
    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
    
}
